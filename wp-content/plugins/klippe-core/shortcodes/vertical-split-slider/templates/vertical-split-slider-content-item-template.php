<div class="mkdf-vss-ms-section" <?php echo klippe_mikado_get_inline_attrs($content_data); ?> <?php klippe_mikado_inline_style($content_style);?>>
	<?php echo do_shortcode($content); ?>
</div>