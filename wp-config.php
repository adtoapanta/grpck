<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'maquina_greenpack');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'root');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '~)eURCOEDPs:xu.wI;vp3K)_;|D^(5;<2~zZXp/ZhftJ SWQB|5&j+o!VMa]OzL.');
define('SECURE_AUTH_KEY',  'WT3CB+D,&SSJWpHfvPQ2q1`}3q62g51J-8%nI>Y>|r.ogME9^f-c%,2zCyus.Gq%');
define('LOGGED_IN_KEY',    'qU_v1Y+h}QqlCIoa_jv34;FE05Lf(ck$:`Kg q<5H6V]_,.Bko. lZK)ol%0,v^j');
define('NONCE_KEY',        'gTF/CVeQkGVuo3qV`//zKOLbPXr]i2Z8Gcm4Y}gYcVL~jMJ1U#D_;q#eT<UVU,t~');
define('AUTH_SALT',        'V[;ur]#26`*Uk?O2UtllmpTsUh0L3m8NDc_hLCfuG6mBcOQlqyWb?F*)3^(D/ tR');
define('SECURE_AUTH_SALT', 'p`ZS52`SPFLYby,33|HJy r{NimK:Pp}Y>]X=#`/DgHsh6j#Aw(KYx*MGdAZh[Iv');
define('LOGGED_IN_SALT',   '#4Uz.r{shZ`hig^!*+MC2x2&&2c(/*[BaU3k-bY$hhi*U]MG7zUC{LQ06ZMz:?@f');
define('NONCE_SALT',       'lX j3}l40w49n%@`K< yx%p~yv)}o!~ l2{h:Y:Xom7=X{zKI>A$c3N6aCXk>%49');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
